#!/usr/bin/env bash
#-*- mode: sh -*-

export PROJECT_ROOT=$(pwd)
export PATH=$PROJECT_ROOT/env:$PATH

function build() {
    local cfile="$1"
    if [[ "-h" == $1 || "--help" == $1 || "$cfile" == "" ]]; then
        echo "Usage: build file"
        echo
        echo "file: C file to build containing 'main' function"
        echo
        return
    fi

    if [ ! -f env/build ]; then
        mkdir -p env/build
    fi

    local executable=$(basename "$cfile")
    executable="${executable%.*}"
    echo "$executable" > env/executable-name

    local cflags="-std=c99 -pedantic-errors -fextended-identifiers -g -x c \
                  -Wno-format-security"

    gcc $cflags -o "env/build/$executable" "$cfile"
}

function run() {
    local executable=$(cat env/executable-name)
    if [ -f "env/build/$executable" ]; then
        "env/build/$executable" "$@"
    fi
}

function debug() {
    local executable=$(cat env/executable-name)

    local debugger=`which kdbg`
    if [[ -f "env/build/$executable" ]] && [[ ! -z "$debugger" ]]; then
        if [[ "" == "$@" ]]; then
            $($debugger "env/build/$executable")
        else
            $($debugger "env/build/$executable" -a "$1 $2")
        fi
    fi
}

function emacs() {
    $(which emacs) --no-init-file --no-splash --no-site-file --no-site-lisp --load env/.emacs "$@"
}
